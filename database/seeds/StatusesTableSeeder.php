<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
        	'name'=> 'availabe'
        ]);

        DB::table('statuses')->insert([
            'name'=> 'unavailable'
        ]);

        DB::table('statuses')->insert([
            'name'=> 'pending'
        ]);

        DB::table('statuses')->insert([
            'name'=> 'approve'
        ]);   

        DB::table('statuses')->insert([
        	'name'=> 'returned'
        ]);   

        DB::table('statuses')->insert([
        	'name'=> 'rejected'
        ]);    

        DB::table('statuses')->insert([
            'name'=> 'cancelled'
        ]);
    }
}
