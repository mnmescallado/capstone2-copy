<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	'name' => 'Sewing Machine',
        	'description' => 'Machine used for sewing'
        ]);

        DB::table('categories')->insert([
        	'name' => 'Needle',
        	'description' => 'Thin piece of polished metal used for sewing'

        ]);

        DB::table('categories')->insert([
        	'name' => 'Scissor',
        	'description' => 'used for cutting fabrics,thread and pattern paper'
        ]);
    }
}
