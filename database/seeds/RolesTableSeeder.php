<?php

use Illuminate\Database\Seeder;
//connect to database
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// select from table roles, insert name = admin
        DB::table('roles')->insert([
        	'name'=>'admin'
        ]);

        DB::table('roles')->insert([
        	'name'=>'user'
        ]);
    }
}
