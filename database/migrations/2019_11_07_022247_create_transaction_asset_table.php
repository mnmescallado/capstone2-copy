<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionAssetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_asset', function (Blueprint $table) {
            $table->bigIncrements('id');

            // $table->date('borrow_date');
            // $table->date('return_date');

            // //assets_id
            // $table->unsignedBigInteger('asset_id');
            // $table->foreign('asset_id')
            //         ->references('id')
            //         ->on('assets')
            //         ->onDelete('restrict')
            //         ->onUpdate('cascade');

            // // transaction_id
            // $table->unsignedBigInteger('transaction_id');
            // $table->foreign('transaction_id')
            //         ->references('id')
            //         ->on('transactions')
            //         ->onDelete('restrict')
            //         ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_asset');
    }
}
