<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('transaction_number');
            $table->date('borrow_date');
            $table->date('return_date');

            //status_id
            $table->unsignedBigInteger('status_id')->default(1);
            $table->foreign('status_id')
                   ->references('id')
                   ->on('statuses')
                   ->onDelete('restrict')
                   ->onUpdate('cascade');

            //users_id
            $table->unsignedBigInteger('user_id')->nullable()->default(null);
            $table->foreign('user_id')
                   ->references('id')
                   ->on('users')
                   ->onDelete('restrict')
                   ->onUpdate('cascade');

            //category_id
            $table->unsignedBigInteger('asset_id')->nullable(true);
            $table->foreign('asset_id')
                    ->references('id')
                    ->on('assets')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');

            $table->softDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
