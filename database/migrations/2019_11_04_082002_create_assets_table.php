<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('serial_number');
            $table->longtext('description');
            $table->string('image');
            $table->integer('quantity');
            //category_id
            $table->unsignedBigInteger('category_id')->nullable(true);
            $table->foreign('category_id')
                    ->references('id')
                    ->on('categories')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
            //status_id
            $table->unsignedBigInteger('status_id')->default(1);
            $table->foreign('status_id')
                   ->references('id')
                   ->on('statuses')
                   ->onDelete('restrict')
                   ->onUpdate('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
