@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12 cold-md-8 mx-auto">
			<h3 class="text-center">
				Edit your Asset
			</h3>
			<hr>

			@if(Session::has('update_failed'))
			<div class="alert alert-warning">
				{{ Session::get ('update_failed') }}
			</div>
			@elseif(Session::has('update_success'))
			<div class="alert alert-success">
				{{ Session::get ('update_success') }}
			</div>
			@endif

			<form action="{{ route ('assets.update', ['asset' => $asset->id]) }}" method="POST" enctype="multipart/form-data">

				@csrf
				@method('PUT')

				<div class="form-group">
					<label class="form-control" for="name">
						Asset name:
					</label>
					<input value="{{ $asset->name }}" id="name" type="text" name="name" class="form-control">

				</div> <!-- end of name input -->

				@if($errors->has('name'))
				<div class="alert alert-danger">
					<small class="mb-0">Product name Required</small>
				</div>
				@endif

				<div class="form-group">
					<label for="serial_number" class="form-control">
						Serial number:
					</label>
					<input value="{{$asset->serial_number}}" type="text" name="serial_number" id="serial_number" class="form-control">

				</div> <!-- end of serial_number -->

				@if($errors->has('serial_number'))
				<div class="alert alert-danger">
					<small class="mb-0">Serial number Required</small>
				</div>
				@endif

				<div class="form-group">
					<label for="category">
						Asset category
					</label>

					<select class="form-control" id="category" name="category">
						@foreach($categories as $category)
						<option value="{{ $category->id }}">
							{{ $category->name }}
						</option>
						@endforeach
						
					</select>

				</div> <!-- end of category -->

				@if($errors->has('category'))
				<div class="alert alert-danger">
					<small class="mb-0">Product category Required</small>
				</div>
				@endif

				<div class="form-group">
					<label for="quantity">
						Quantity:
					</label>
					<input value="{{$asset->quantity}}" type="number" name="quantity" id="quantity" class="form-control" min="1">

				</div> {{-- end of quantity input --}}

				@if($errors->has('quantity'))
				<div class="alert alert-danger">
					<small class="mb-0">Quantity is Required</small>

				</div> 
				@endif

				<div class="form-group">
					<label for="image">
						Product image:
					</label>
					<input type="file" name="image" id="image" class="form-control">		

				</div> <!-- end of image input -->

{{-- 				@if($errors->has('image'))
				<div class="alert alert-danger">
					<small class="mb-0">Product image Required Check if images is not greater that 2.00mb</small>
				</div>
				@endif --}}

				<div class="form-group">
					<label for="description">
						Product description
					</label>
					<textarea
					id="description"
					class="form-control"
					name="description"
					cols="30"
					rows = "10">
					{{$asset->description}}
				</textarea>

			</div> <!-- end of description input -->

			@if($errors->has('description'))
			<div class="alert alert-danger">
				<small class="mb-0">Product description Required</small>
			</div>
			@endif

			<button class="btn btn-primary btn-block">
				Edit asset
			</button>




		</form> {{-- end of from --}}



	</div> {{-- end of col --}}


</div> {{-- end of row --}}


</div> {{-- end of caontainer --}}

@endsection