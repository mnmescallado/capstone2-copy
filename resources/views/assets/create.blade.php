@extends('layouts.app')


@section('content')

<div class="container">
	<div class="row">
		<div class="col-12 col-md-6 mx-auto">

			<h3 class="text-center">
				Add Asset
			</h3>		
			<hr>

			<form action="{{ route('assets.store') }}" method="POST" enctype="multipart/form-data">
				@csrf

				<div class="form-group">
					<label class="form-control" for="name">
						Asset name:
					</label>
					<input id="name" type="text" name="name" class="form-control">

				</div> <!-- end of name input -->

				@if($errors->has('name'))
				<div class="alert alert-danger">
					<small class="mb-0">Product name Required</small>
				</div>
				@endif

				<div class="form-group">
					<label for="serial_number" class="form-control">
						Serial number:
					</label>
					<input type="text" name="serial_number" id="serial_number" class="form-control">

				</div> <!-- end of serial_number -->

				@if($errors->has('serial_number'))
				<div class="alert alert-danger">
					<small class="mb-0">Serial number Required</small>
				</div>
				@endif

				<div class="form-group">
					<label for="category">
						Asset category
					</label>

					<select class="form-control" id="category" name="category">
						@foreach($categories as $category)
						<option value="{{ $category->id }}">
							{{ $category->name }}
						</option>
						@endforeach
						
					</select>

				</div> <!-- end of category -->

				@if($errors->has('category'))
				<div class="alert alert-danger">
					<small class="mb-0">Product category Required</small>
				</div>
				@endif

				<div class="form-group">
					<label for="quantity">
						Quantity:
					</label>
					<input type="number" name="quantity" id="quantity" class="form-control" min="1">

				</div> {{-- end of quantity input --}}

				@if($errors->has('quantity'))
				<div class="alert alert-danger">
					<small class="mb-0">Quantity is Required</small>

				</div> 
				@endif

				<div class="form-group">
					<label for="image">
						Product image:
					</label>
					<input type="file" name="image" id="image" class="form-control">		

				</div> <!-- end of image input -->

				@if($errors->has('image'))
				<div class="alert alert-danger">
					<small class="mb-0">Product image Required Check if images is not greater that 2.00mb</small>
				</div>
				@endif

				<div class="form-group">
					<label for="description">
						Product description
					</label>
					<textarea
					id="description"
					class="form-control"
					name="description"
					cols="30"
					rows = "10">
				</textarea>

			</div> <!-- end of description input -->

			@if($errors->has('description'))
			<div class="alert alert-danger">
				<small class="mb-0">Product description Required</small>
			</div>
			@endif

			<button class="btn btn-primary btn-block">
				Add asset
			</button>

		</form>	<!-- end of form -->

	</div> <!-- end of column add asset -->

	<div class="col-12 col-md-6 mx-auto">

		@if (Session::has('category_message'))
		<div class="alert alert-success">
			{{Session::get ('category_message') }}
		</div>
		@endif

		@if($errors->has('add_category'))
		<div class="alert alert-secondary">
			Category not added
		</div>
		@endif

		<h3 class="text-center">
			Add category
		</h3>
		<hr>

		<form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
			@csrf

			<div class="form-group">
				<label class="form-control" for="category_name">
					Category name:
				</label>
				<input type="text" name="category_name" id="category_name" class="form-control">
			</div>

			@if($errors->has('category_name'))
			<div class="alert alert-danger">
				<small class="mb-0">Category name Required</small>
			</div>
			@endif

			<div class="form-group">
				<label class="form-control" for="category_image">
					Category image:
				</label>
				<input type="file" name="category_image" id="category_image" class="form-control">
			</div>

			@if($errors->has('image'))
			<div class="alert alert-danger">
				<small class="mb-0">Product image Required Check if images is not greater that 2.00mb</small>
			</div>
			@endif

{{-- 				<div class="form-group">
					<label class="form-control" for="category_name">
						Category quantity:
					</label>
					<input type="text" name="category_name" id="category_name" class="form-control">
				</div> --}}

				<div class="form-group">
					<label for="category_desc">
						Category description:
					</label>
					<textarea
					id="category_desc"
					class="form-control"
					name="category_desc"
					cols="30"
					rows = "10">
				</textarea>

			</div>

			@if($errors->has('category_desc'))
			<div class="alert alert-danger">
				<small class="mb-0">Category Description is Required</small>
			</div>
			@endif




			<button type="submit" class="btn btn-primary btn-block">Add category</button>


		</form>

	</div> <!-- end of column add category -->


</div> <!-- end of row -->		

</div> <!-- end of container -->


@endsection

