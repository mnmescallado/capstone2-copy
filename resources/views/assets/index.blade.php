@extends('layouts.app')


@section('content')

<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="jumbotron">
				<h1 class="text-center">
					Choose your equipment!!!
				</h1>

			</div> {{-- end of jumbotron --}}

		</div>{{-- end of column --}}	

	</div> {{-- end of row upper--}}

	<div class="row">
		<div class="col-12 col-md-12 mx-auto">
			<div class="row">
				@foreach($assets as $asset)
				<div class="col-12 col-md-3 mx-auto">
					{{-- start of card --}}
					<div class="card">
						<img src="{{ url('/public/' . $asset->image) }}" class="card-img-top card-image mx-auto mt-1 img-fluid img-thumbnail" 
						alt="Responsive image">

						<div class="card-body">
							<h4 class="card-title">
								{{$asset->name}}
							</h4>

							<p class="card-text">
								{{$asset->description}}
							</p>

							<p class="card-text">
								{{$asset->quantity}}
							</p>			

						</div> {{-- end of card-body --}}			
						<div class="card-foot">
							@can('isAdmin')
							<a href="{{ route ('assets.edit', ['asset' => $asset->id] ) }}" class="btn btn-success my-1 btn-sm rounded btn-block">Edit Asset</a>

							<form action="{{ route ('assets.destroy', ['asset' => $asset->id]) }}" method="POST">
								@csrf
								@method('DELETE')
								<button class="btn btn-danger btn-sm rounded btn-block">Delete Asset</button>
							</form>
							@endcannot			

						</div> {{-- end of card-foot --}}

					</div> {{-- end of card --}}
				</div>
				@endforeach

			</div>

		</div> {{-- end of column --}}		

	</div> {{-- end of lower row --}}

</div> {{-- end of caontainer --}}


@endsection