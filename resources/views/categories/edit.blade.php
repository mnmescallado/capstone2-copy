@extends('layouts.app')


@section('content')

<div class="container">
	<div class="row">
		<div class="col-12 cold-md-8 mx-auto">
			<h3 class="text-center">
				Edit Category
			</h3>
			<hr>

			@if(Session::has('update_failed'))
			<div class="alert alert-warning">
				{{ Session::get ('update_failed') }}
			</div>
			@elseif(Session::has('update_success'))
			<div class="alert alert-success">
				{{ Session::get ('update_success') }}
			</div>
			@endif

			<form action="{{ route ('categories.update', ['category' => $category->id]) }}" method="POST" enctype="multipart/form-data">
				
				@csrf
				@method('PUT')

				{{-- name input --}}
				<div class="form-group">
					<label for="category_name">
					Category name:
					</label>
					<input id="category_name" type="text" name="category_name" class="form-control" value="{{$category->name}}">					

				</div> {{-- end of name input --}}

				@if($errors->has('category_name'))
				<div class="alert alert-danger">
					<small class="mb-0">Category name Required</small>
				</div>
				@endif

				<div class="form-group">
					<label for="category_image">Category image:</label>
					<input id="category_image" type="file" name="category_image" class="form-control-file">
				</div> {{-- end of category images --}}

				@if($errors->has('category_image'))
				<div class="alert alert-danger">
					<small class="mb-0">Category image Required Check if images is not greater that 2.00mb</small>
				</div>
				@endif

				<div class="form-group">
					<label for="category_description">Product Description</label>
					<textarea 
					id="category_description" 
					class="form-control" 
					name="category_description" 
					cols="30" 
					rows="10">{{ $category->description }}</textarea>
				</div> {{-- end of category description --}}

				@if($errors->has('category_description'))
				<div class="alert alert-danger">
					<small class="mb-0">Category description Required</small>
				</div>
				@endif

				<button class="btn btn-primary btn-block">Edit Category</button>






			</form> {{-- end of form --}}


			

		</div> {{-- end of column --}}


	</div> {{-- end of row --}}


</div> {{-- end of container --}}

@endsection