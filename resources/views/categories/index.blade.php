@extends('layouts.app')


@section('content')
{{-- {{dd($assets)}} --}}
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="jumbotron">
				<h1 class="text-center">
					Choose your equipment!!!
				</h1>

			</div> {{-- end of jumbotron --}}

		</div>{{-- end of column --}}	

	</div> {{-- end of row upper--}}

	@if(Session::has('destroy_message'))
	<div class="alert alert-danger">
		{{ Session::get ('destroy_message') }}
	</div>
	@endif

	<div class="row">
		<div class="col-12 col-md-12 mx-auto">
			<div class="row">
				@foreach($categories as $category)
				<div class="col-12 col-md-6 my-2">
					{{-- start of card --}}
					<div class="card">
						<img src="{{ url('/public/' . $category->image) }}" 
						class="card-img-top card-image mx-auto mt-1 img-fluid img-thumbnail" 
						alt="Responsive image"
						>
						<div class="card-body">
							<h4 class="card-title">
								{{$category->name}}
							</h4>
							<p class="card-text">
								{{$category->description}}
							</p>		
						</div> {{-- end of card-body --}}			
						<div class="card-foot">

							<div class="accordion" id="accordionExample">
								<div class="card">
									<div class="card-header" id="headingOne">
										<h2 class="mb-0">
											<button class="btn btn-link" 
											type="button" 
											data-toggle="collapse" 
											data-target="#category-{{$category->id}}" 
											aria-expanded="true" 
											aria-controls="collapseOne">
											Asset Unit
										</button>
									</h2>
								</div>


								<div id="category-{{$category->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
									<div class="card-body">
										<div class="accordion container" id="accordionExample1">
											<div class="container">
												<div class="row">
													<div class="col-12">
														<div class="row">

															@foreach($assets as $asset)
															@if($category->id == $asset->category_id)
															<div class="col-12 my-1">
																<div class="card">
																	<div class="card-header" id="headingOne1">
																		<h2 class="mb-0">
																			<button 
																			class="btn btn-link" 
																			type="button" 
																			data-toggle="collapse" 
																			data-target="#asset-{{ $asset->id }}"
																			aria-expanded="true" 
																			aria-controls="collapseOne">
																			<img src="{{ url('/public/' . $asset->image) }}" 
																			class="img-fluid img-thumbnail" 
																			alt="Responsive image">

																			{{ $asset->name }}
																		</button>
																	</h2>
																</div>
																<div id="asset-{{ $asset->id }}" 
																	class="collapse" 
																	aria-labelledby="headingOne1" 
																	data-parent="#accordionExample1">
																	<div class="card-body">
																		@can('isAdmin')
																		<a href="{{ route ('assets.edit', ['asset' => $asset->id] ) }}" class="btn btn-success my-1 btn-sm rounded btn-block">Edit Asset</a>

																		<form action="{{ route ('assets.destroy', ['asset' => $asset->id]) }}" method="POST">
																			@csrf
																			@method('DELETE')
																			<button class="btn btn-danger my-1 btn-sm rounded btn-block">Delete Asset</button>
																		</form>
																		@endcan

																		<form action="{{route('transactions.store')}}" method="POST">
																			{{-- goes to store- its the confirmation --}}
																			@csrf
																			<input type="hidden" name="asset_id" value="{{$asset->id}}">

																			<label for="borrow">Borrow date:</label>
																			<input type="date" name="borrow" id="borrow" class="form-control">
																			<label for="return">Return date:</label>
																			<input type="date" name="return" id="return" class="form-control">


																			<button 
																			{{-- @cannot('isLogged') --}}
																			{{-- type="button" data-toggle="modal"  --}}
																			{{-- data-target="#authentication-modal"	 --}}
																			{{-- default is submit --}}
																			{{-- @endcannot --}}
																			class="btn btn-info btn-sm btn-block rounded my1">Request</button>
																		</form>
																	</div>
																</div>
															</div>
														</div>
														@endif
														@endforeach

													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>




					</div>

					{{-- edit category button --}}
					@can('isAdmin')
					<a href="{{ route ('categories.edit', ['category' => $category->id] ) }}" class="btn btn-warning btn-block my-1 rounded">Edit Category</a>

					{{-- delete category button --}}
					<form action="{{ route ('categories.destroy', ['category' => $category->id]) }}" method="POST">
						@csrf
						@method('DELETE')

						<button class="btn btn-danger btn-block rounded">Delete Category</button>
					</form>
					@endcan					

				</div> {{-- end of card-foot --}}

			</div> {{-- end of card --}}
		</div>
		@endforeach

	</div>

</div> {{-- end of column --}}		

</div> {{-- end of lower row --}}

</div> {{-- end of caontainer --}}


@endsection