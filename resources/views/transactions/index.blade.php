@extends('layouts.app')


@section('content')
{{-- for pending --}}
{{-- {{dd($pending_transactions)}} --}}
{{--             @foreach($transactions as $trans)
            @foreach($trans->assets as $ass)
            {{dd($ass)}}
            @endforeach
            @endforeach --}}
            <div class="contrainer">

              <h3 class="text-center">Transaction Pending Approval</h3>
              <div class="row">
                <div class="col-12 col-md-11 mx-auto">

                  <table class="table table-dark">
                    <thead>
                      <tr>
                        <th scope="col">User</th>
                        <th scope="col">Category Line</th>
                        <th scope="col">Borrow Date</th>
                        <th scope="col">Return Date</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>

                      @foreach($pending_transactions as $tran)
                      {{-- @foreach($trans->assets as $ass) --}}
                      {{-- {{dd($tran)}} --}}
                      {{-- {{dd($tran->assets)}} --}}
                      <tr class="table-active">
                        <td scope="row">{{$tran->user->name}}</td>
                        {{-- {{dd($pending_transactions)}} --}}
                        {{-- @foreach($tran as $ass) --}}
                        {{-- {{dd($ass)}} --}}
                        {{-- @endforeach --}}
                        {{-- {{$tran->assets->name}} --}}
                        {{-- <td>{{$tran->transaction_number}}</td> --}}
                        <td>{{$tran->asset->category->name}}</td>
                        {{-- @endforeach --}}
                        <td>{{$tran->borrow_date}}</td>
                        <td>
                          {{$tran->return_date}}
                        </td>
                        {{-- @endforeach --}}

                        {{-- <td>{{$trans->category->description}}</td> --}}

                        {{-- @endforeach --}}
                        <td>{{$tran->status->name}}</td>
                        <td>

                          @can('isAdmin')
                          <form action="{{ route('transactions.update',['transaction' => $tran->id])}}" method="POST">

                            @csrf
                            @method('PUT')
                            <input type="hidden" name="status_id" value="4">
                            <button class="btn btn-success approveBtn form-control">Approve</button>
                          </form>

                          <form action="{{ route('transactions.destroy',['transaction' => $tran->id])}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="status_id" value="6">
                            <button class="btn btn-danger form-control">Reject</button>
                          </form>
                          @endcan

                          @cannot('isAdmin')
                          <form action="{{ route('transactions.destroy',['transaction' => $tran->id])}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" name="status_id" value="7">
                            <button class="btn btn-warning form-control">Cancel</button>
                          </form>
                          @endcannot

                        </td>
                      </tr>
                      {{-- @endforeach --}}
                      @endforeach

                      {{-- {{dd(Session::all("fdsafdsa.$tran", '$tran->category.id'))}} --}}

                    </tbody>
                  </table> 

                </div> {{-- end of col --}}
              </div> {{-- end of row --}}
            </div>{{-- end of container --}}

            {{-- for approved --}}
            <hr>

            <div class="contrainer">
              <h3 class="text-center">Approved Transactions</h3>
              <div class="row">
                <div class="col-12 col-md-11 mx-auto">

                  <table class="table table-dark">
                    <thead>
                      <tr>
                        <th scope="col">User</th>
                        <th scope="col">Transaction Number</th>
                        <th scope="col">Asset</th>
                        <th scope="col">Borrow Date</th>
                        <th scope="col">Return Date</th>
                        <th scope="col">Approve On</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>

                      @foreach($approved_transactions as $tran)
                      <tr class="table-active">
                        <th scope="row">{{$tran->user->name}}</th>
                        <td>{{$tran->transaction_number}}</td>
                        <td>
                          {{$tran->asset->name}}
                        </td>
                        <td>
                          {{$tran->borrow_date}}
                        </td>
                        <td>{{$tran->return_date}}</td>
                        <td>{{$tran->updated_at}}</td>
                        <td>
                          <form action="{{ route('transactions.update',['transaction' => $tran->id])}}" method="POST">
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="status_id" value="5">
                            <button class="btn btn-success approveBtn form-control">Tag as Returned</button>
                          </form>
                        </td>
                      </tr>
                      {{-- @endif --}}
                      @endforeach
                    </tbody>
                  </table> 

                </div> {{-- end of col --}}
              </div> {{-- end of row --}}
            </div>{{-- end of container --}}

            <hr>
            {{-- for completed --}}

            <div class="contrainer">
              <h3 class="text-center">Completed Transactions</h3>
              <div class="row">
                <div class="col-12 col-md-11 mx-auto">

                  <table class="table table-dark">
                    <thead>
                      <tr>
                        <th scope="col">User</th>
                        <th scope="col">Transaction Number</th>
                        <th scope="col">Asset</th>
                        <th scope="col">Borrow Date</th>
                        <th scope="col">Return Date</th>
                        <th scope="col">Status</th>
                        <th scope="col">Completed On</th>
                      </tr>
                    </thead>
                    <tbody>

                      @foreach($completed_transactions as $tran)
                      <tr class="table-active">
                        <th scope="row">{{$tran->user->name}}</th>
                        <td>{{$tran->transaction_number}}</td>
                        <td>
                          {{$tran->asset->name}}
                        </td>
                        <td>
                          {{$tran->borrow_date}}
                        </td>
                        <td>{{$tran->return_date}}</td>
                        <td>{{$tran->status->name}}</td>
                        <td>{{$tran->updated_at}}</td>
                      </tr>
                      {{-- @endif --}}
                      @endforeach
                    </tbody>
                  </table> 

                </div> {{-- end of col --}}
              </div> {{-- end of row --}}
            </div>{{-- end of container --}}

            <hr>
            {{-- for completed --}}

            <div class="contrainer">
              <h3 class="text-center">Cancelled Transactions</h3>
              <div class="row">
                <div class="col-12 col-md-11 mx-auto">

                  <table class="table table-dark">
                    <thead>
                      <tr>
                        <th scope="col">User</th>
                        <th scope="col">Transaction Number</th>
                        <th scope="col">Asset</th>
                        <th scope="col">Borrow Date</th>
                        <th scope="col">Return Date</th>
                        <th scope="col">Status</th>
                        <th scope="col">Cancelled On</th>
                      </tr>
                    </thead>
                    <tbody>

                      @foreach($cancelled_transactions as $tran)
                      <tr class="table-active">
                        <th scope="row">{{$tran->user->name}}</th>
                        <td>{{$tran->transaction_number}}</td>
                        <td>
                          {{$tran->asset->name}}
                        </td>
                        <td>
                          {{$tran->borrow_date}}
                        </td>
                        <td>{{$tran->return_date}}</td>
                        <td>{{$tran->status->name}}</td>
                        <td>{{$tran->updated_at}}</td>
                      </tr>
                      {{-- @endif --}}
                      @endforeach
                    </tbody>
                  </table> 

                </div> {{-- end of col --}}
              </div> {{-- end of row --}}
            </div>{{-- end of container --}}

            @endsection


{{-- <tr>
            <th scope="row">Default</th>
            <td>Column content</td>
            <td>Column content</td>
            <td>Column content</td>
          </tr>
          <tr class="table-primary">
            <th scope="row">Primary</th>
            <td>Column content</td>
            <td>Column content</td>
            <td>Column content</td>
          </tr>
          <tr class="table-secondary">
            <th scope="row">Secondary</th>
            <td>Column content</td>
            <td>Column content</td>
            <td>Column content</td>
          </tr>
          <tr class="table-success">
            <th scope="row">Success</th>
            <td>Column content</td>
            <td>Column content</td>
            <td>Column content</td>
          </tr>
          <tr class="table-danger">
            <th scope="row">Danger</th>
            <td>Column content</td>
            <td>Column content</td>
            <td>Column content</td>
          </tr>
          <tr class="table-warning">
            <th scope="row">Warning</th>
            <td>Column content</td>
            <td>Column content</td>
            <td>Column content</td>
          </tr>
          <tr class="table-info">
            <th scope="row">Info</th>
            <td>Column content</td>
            <td>Column content</td>
            <td>Column content</td>
          </tr>
          <tr class="table-light">
            <th scope="row">Light</th>
            <td>Column content</td>
            <td>Column content</td>
            <td>Column content</td>
          </tr>
          <tr class="table-dark">
            <th scope="row">Dark</th>
            <td>Column content</td>
            <td>Column content</td>
            <td>Column content</td>
          </tr> --}}