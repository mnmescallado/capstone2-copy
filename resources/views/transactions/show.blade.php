@extends('layouts.app')

@section('content')
{{-- {{$transaction}} --}}
{{-- {{dd(Session::get('transaction'))}} --}}
<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<h3 class="text-center">Transaction Details</h3>
			<hr>
		</div> {{-- end of col --}}		
	</div> {{-- end of row --}}

	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<div class="table-reponsive">
				<table class="table table-sm table-borderless">

					<tbody>
						<tr>
							<td>Customer Name:</td>
							<td> <strong>{{$transaction->user->name}}</strong> </td>

						</tr>

						<tr>
							<td>Transaction Number:</td>
							<td><strong> {{strtoupper($transaction->transaction_number)}} </strong></td>

						</tr>

						<tr>
							<td>Status:</td>
							<td>{{$transaction->status->name}}</td>

						</tr>

						<tr>
							<td>Request Date:</td>
							<td>{{$transaction->created_at->format('d-F-Y m:i:s')}}</td>

						</tr>
						{{-- remove pivot --}}
					{{-- @foreach($transaction->assets as $transaction_asset) --}}
					{{-- @endforeach --}}
						<tr>
							<td>Borrow Date:</td>
							<td>{{$transaction->borrow_date}}</td>

						</tr>

						<tr>
							<td>Return Date:</td>
							<td>{{$transaction->return_date}}</td>

						</tr>

					</tbody>

				</table>


			</div>
		</div> {{-- end of 2nd col --}}
	</div> {{-- end of 2nd row --}}
</div> {{-- end of containter --}}

@endsection