<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset extends Model
{
	use SoftDeletes;
	public function category() 
    {
    	return $this->belongsTo('App\Category');
    }

    public function transactions()
    {
    	return $this->hasMany('App\Transaction');
    		// ->withPivot('borrow_date','return_date')
      //       ->withTimestamps();
        // return $this->belongsToMany('App\Transaction');
    }
}
