<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function status()
    {
    	return $this->belongsTo('App\Status');
    }

    public function user() 
    {
    	return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function asset()
    {
        return $this->belongsTo('App\Asset');
    }











    

    // set many to many relationship of transaction(no migrations) and product
    public function assets() 
    {
        return $this->belongsToMany('App\Asset','transaction_asset');
                        //set pivot table excluding the foreign key with timestamps
                      // ->withPivot('borrow_date','return_date')
                      // ->withTimestamps();

        // return $this->belongsToMany('App\Asset');


    }

}
