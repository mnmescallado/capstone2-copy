<?php

namespace App\Http\Controllers;

use App\Asset;
use App\Category;
use App\Transaction;
use App\Status;
use Illuminate\Http\Request;
use Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $assets = Asset::all();
        $transactions = Transaction::all();
        return view('categories.index')
            ->with('categories', $categories)
            ->with('assets', $assets)
            ->with('transactions', $transactions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo "hi from category - store";
        $request->validate ([
            'category_name' => 'required|unique:categories,name',
            'category_image' => 'required|image|max:20000',
            'category_desc' => 'required|string'
        ]);

        $file = $request->file('category_image');

        $file_name = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);
     
        $file_extension = $file->extension();

        $random_chars = Str::random(5);

        $new_filename = date('Y-m-d-H-i-s') . "_" . $random_chars . "_" . $file_name . "." . $file_extension;

        $filepath = $file->storeAs('images', $new_filename, 'public');


        $name = $request->input('category_name');
        $description = $request->input('category_desc');
        $image = $filepath;

        $category = new Category;
        $category->name = $name;
        $category->description = $description;
        $category->image = $image;
        // $category->quantity = 10;
        $category->save();

        $request->session()->flash('category_message','category successfully added');
        return redirect(route('assets.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        // echo "hi from category edit";
        $assets = Asset::all();
        return view('categories.edit')
            ->with('assets', $assets)
            ->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        // dd($request->input('category_name'));
        // echo "hi from categories update";
        $request->validate ([
            'category_name' => 'required|unique:categories,name',
            'category_description' => 'required|string'
        ]);

        if(!$request->hasFile('category_image') && 
            $category->name == $request->input('category_name') &&
            $category->description == $request->input('category_description')) 
        {
            $request->session()->flash('update_failed', 'No changes made | Something went wrong');
        }


        else {
            if($request->hasFile('category_image'))
            {
                $request->validate([
                    'category_image' => 'required|image|max:20000',
                ]);

                $file =$request->file('category_image');

                $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

                $file_extension = $file->extension();

                $random_chars = Str::random(5);

                $new_filename = date('Y-m-d-H-i-s') . "_" . $random_chars . "_" . $file_name . "." . $file_extension;

                $filepath = $file->storeAs('images', $new_filename,'public');

                $category->image = $filepath;

            }

            $category->name = $request->input('category_name');
            $category->description = $request->input('category_description');
            // $category->quantity = 10;
            $category->save();

            $request->session()->flash('update_success', 'Category successfully updated');

        }

        return redirect (route ('categories.edit',['category'=> $category->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        // echo "from category destroy";
        $category->delete();

        return redirect(route('categories.index'))->with('destroy_message','Category has been deleted');
    }
}
