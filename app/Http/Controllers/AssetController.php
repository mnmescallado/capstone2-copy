<?php

namespace App\Http\Controllers;

use App\Status;
use App\Asset;
use App\Category;
use Illuminate\Http\Request;
use Str;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // echo "hi from asset - index";
        $assets = Asset::all();
        // dd($assets);
        return view('assets.index')->with('assets', $assets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Asset $asset)
    {
        // echo 'hi from assets - create';
        $categories = Category::all();
        return view('assets.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Asset $asset)
    {

        // echo 'hello fron assets store';
        $request->validate([
            'name'=>'required|string',
            'serial_number'=>'required|string',
            'description'=>'required|string',
            'image'=>'required|image|max:20000',
            'quantity'=>'required|numeric'
        ]);

        $file = $request->file('image');

        $file_name = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);
        // dd($file);

        $file_extension = $file->extension();

        $random_chars = Str::random(5);

        $new_filename = date('Y-m-d-H-i-s') . "_" . $random_chars . "_" . $file_name . "." . $file_extension;

        $filepath = $file->storeAs('images', $new_filename,'public');

        $name = $request->input('name');
        $serial_number = $request->input('serial_number');
        $description = $request->input('description');
        $image = $filepath;
        $quantity = $request->input('quantity');
        $category_id = $request->input('category');
        // dd($image);

        $asset = new Asset;
        $asset->name = $name;
        $asset->serial_number = $serial_number;
        $asset->description = $description;
        $asset->image = $image;
        $asset->quantity = $quantity;
        $asset->category_id = $category_id;
        $asset->save();

        return redirect(route('assets.show',['asset'=>$asset->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        return view('assets.show')->with('asset', $asset);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
    // echo "hi from assets-edit";
        $categories = Category::all();
        return view('assets.edit')
        ->with('asset',$asset)
        ->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        // echo "hi from asets-update";
        $request->validate([
            'name'=>'required|string',
            'serial_number'=>'required|string',
            'description'=>'required|string',
            // 'image'=>'required|image|max:20000',
            'quantity'=>'required|numeric'
        ]);

        if(!$request->hasFile('image') && 
            $asset->name == $request->input('name') &&
            $asset->description == $request->input('description') &&
            $asset->quantity == $request->input('quantity') &&
            $asset->category_id == $request->input('category') ) 
        {
            $request->session()->flash('update_failed', 'No changes made | Something went wrong');
        }

        else {

            if($request->hasFile('image'))
            {
                $request->validate([
                    'image' => 'required|image|max:20000'
                ]);

                $file = $request->file('image');

                $file_name = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME);

                $file_extension = $file->extension();

                $random_chars = Str::random(5);

                $new_filename = date('Y-m-d-H-i-s') . "_" . $random_chars . "_" . $file_name . "." . $file_extension;

                $filepath = $file->storeAs('images', $new_filename,'public');

                $asset->image = $filepath;
            }

            $asset->name = $request->input('name');
            $asset->serial_number = $request->input('serial_number');
            $asset->description = $request->input('description');
            $asset->quantity = $request->input('quantity');
            $asset->category_id = $request->input('category');

            $asset->save();

            $request->session()->flash('update_success', 'Asset succesfully Updated');

        }
        return redirect(route('assets.edit',['asset'=>$asset->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        // echo "bye bye bye";
        $asset->delete();

        return redirect(route('categories.index'))->with('destroy_message','Asset has been deleted');
    }
}
