<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;

use Auth;
use Str;
use Session;
use App\Asset;
use App\Status;
use App\User;
use App\Category;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // echo "welcome to transactions";

        if (Auth::user()->role_id == 1) {
        $transactions = Transaction::all();
        $pending_transactions = Transaction::all()->where('status_id', 3);
        $approved_transactions = Transaction::all()->where('status_id', 4);
        $completed_transactions = Transaction::all()->whereIn('status_id', [5,6]);
        $cancelled_transactions = Transaction::all()->where('status_id', 7);
        
        
        // admin can see all transactions
        }
        else {
        $transactions = Transaction::all();
        $pending_transactions = Transaction::all()->where('status_id', 3)->where('user_id', Auth::user()->id);
        $approved_transactions = Transaction::all()->where('status_id', 4)->where('user_id', Auth::user()->id);
        $completed_transactions = Transaction::all()->whereIn('status_id', [5,6])->where('user_id', Auth::user()->id);
        $cancelled_transactions = Transaction::all()->where('status_id', 7)->where('user_id', Auth::user()->id);
        // only transaction where the login user_id only sees
        // $transactions = Transaction::where('user_id', Auth::user()->id)->get();
        }

        $statuses = Status::all();
        $categories = Category::all();
        $assets = Asset::all();

        // dd($transactions);
        return view('transactions.index')
            ->with('transactions', $transactions)
            ->with('categories', $categories)
            ->with('statuses', $statuses)
            ->with('assets', $assets)
            ->with('pending_transactions', $pending_transactions)
            ->with('approved_transactions', $approved_transactions)
            ->with('completed_transactions', $completed_transactions)
            ->with('cancelled_transactions', $cancelled_transactions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        $assets=Asset::all();
        // echo "from transaction -store";
        $transaction = new Transaction;

        $transaction_number = Auth::user()->id . Str::random(5) . time();


        $user_id = Auth::user()->id;
        // status_id set to pending
        $status_id = 3;

        $transaction->borrow_date = $request->input('borrow');

        $transaction->return_date = $request->input('return');

        $transaction->asset_id = $request->input('asset_id');

        $transaction->transaction_number = $transaction_number;
        $transaction->user_id = $user_id;
        $transaction->status_id = $status_id;
        $transaction->save();

        // $request->validate([
        //     'id' => 'required',
        //     'quantity' => 'default(1)'
        // ]);

        // $asset_quantity = 5;
        // $id = $request->input('category_id');

        // $request->session()->put("trans.$id", $asset_quantity);

        // $trans_ids = array_keys(Session::get('trans'));

        // $trans = Transaction::find($trans_ids);
        // dd($trans);


        // pivot table
        // foreach($assets as $asset)
        // {
        // $transaction->assets()->attach($asset->id, [
        //     'borrow_date' => $request->input('borrow'),
        //     'return_date' => $request->input('return')
        // ]);
        // }

        // dd($transaction->assets);
        return redirect(route('transactions.show',['transaction'=> $transaction->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        // echo "hello from transaction store";
        // dd($transaction);
        return view('transactions.show')->with('transaction', $transaction);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        // echo "hello from transaction update";
        $transaction->status_id = $request->input('status_id');
        $transaction->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction, Request $request)
    {
        // echo "string";
        $transaction->status_id = $request->input('status_id');
        $transaction->save();
        return redirect()->back();
    }
}
